#!/bin/bash
curl -L -O https://github.com/GoogleCloudPlatform/gcsfuse/releases/download/v0.39.2/gcsfuse_0.39.2_amd64.deb
dpkg --install gcsfuse_0.39.2_amd64.deb
sleep 10
gcsfuse --debug_gcs --key-file llave.json fiesta-tequila-bing-2014 /app/data/images/
