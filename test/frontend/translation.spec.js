"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const ProjectPath_1 = require("../../src/backend/ProjectPath");
const fs_1 = require("fs");
const chai = require('chai');
const path = require("path");
const { expect } = chai;
describe('UI', () => {
    it('translation should be listed in the angular.json', () => __awaiter(void 0, void 0, void 0, function* () {
        const base = path.join('src', 'frontend', 'translate');
        const translations = yield fs_1.promises.readdir(path.join(ProjectPath_1.ProjectPath.Root, base));
        const angularConfig = require(path.join(ProjectPath_1.ProjectPath.Root, 'angular.json'));
        const knownTranslations = angularConfig.projects.pigallery2.i18n.locales;
        for (const t of translations) {
            const lang = t.substr(t.indexOf('.') + 1, 2);
            if (lang === 'en') {
                continue; // no need to add 'en' as it is the default language.
            }
            const translationPath = path.join(base, t).replace(new RegExp('\\\\', 'g'), '/');
            expect(knownTranslations[lang]).to.deep.equal({
                baseHref: '',
                translation: translationPath
            }, translationPath + ' should be added to angular.json');
        }
    }));
});
//# sourceMappingURL=translation.spec.js.map